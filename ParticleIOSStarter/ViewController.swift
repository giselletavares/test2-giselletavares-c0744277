//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    // MARK: User variables
    let USERNAME = ProcessInfo.processInfo.environment["USERNAME"]
    let PASSWORD = ProcessInfo.processInfo.environment["PASSWORD"]
    
    // MARK: Device
    let DEVICE_ID = ProcessInfo.processInfo.environment["DEVICE_ID"]
    var myPhoton : ParticleDevice?

    // MARK: Other variables
    var timeElapsed:Int = 0
    var timer = Timer()
    var maxDuration:Int = 20
    var duration:Double = 1.0
    var mohammadAmount: Int = 0
    var countMohammadTime: Int = 0

    
    // MARK: Outlets
    @IBOutlet weak var lblTimeElapsed: UILabel!
    @IBOutlet weak var lblTimeslow: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1. Initialize the SDK
        ParticleCloud.init()
 
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME!, password: self.PASSWORD!) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")

                // try to get the device
                self.getDeviceFromCloud()

            }
        } // end login
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID!) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
//                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    
    @IBAction func startMonitoringButton(_ sender: UIButton) {
        
        // timer on the phone
        self.timeElapsed = 0
        
        // call the mood timer
        self.moodTimer()
        
        // start the particle
        self.startTheParticle()
        
    }
    
    func moodTimer(){
        self.timer.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: self.duration, repeats: true) { (Timer) in
            print("self.timeElapsed: \(self.timeElapsed)")
            print("self.duration: \(self.duration)")
            
            self.lblTimeElapsed.text = ""
            self.lblTimeElapsed.text = "\(self.timeElapsed)"
            self.timeElapsed += 1

            if(self.timeElapsed >= self.maxDuration + 1){
                Timer.invalidate()
                self.timeElapsed = 20
                self.lblTimeElapsed.text = "\(self.timeElapsed)"
            }
            
        }
    }
    
    func startTheParticle(){
        var params = ["start"]
        // Send score to Particle
        // ------------------------------
        if self.mohammadAmount > 0 {
            params = ["\(self.mohammadAmount) \(self.timeElapsed)"]
        }
        
        var task = myPhoton!.callFunction("mood", withArguments: params) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("started")
            } else {
                print("Error when telling Particle to start")
            }
        }
    }
    
    func stopTheParticle(){
        var params = ["stop"]
        
        var task = myPhoton!.callFunction("mood", withArguments: params) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("stop")
            } else {
                print("Error when telling Particle to stop")
            }
        }
    }
    
    @IBAction func amountMohammad(_ sender: UISlider) {
        self.mohammadAmount = Int(sender.value)
        self.lblTimeslow.text = "Time to slows down by: \(self.mohammadAmount) s"
        
        if self.mohammadAmount > 0 && self.timeElapsed < 20 {
            self.duration = 1.0 + Double(self.mohammadAmount)
            print("duration:  \(self.duration)")
            
            self.stopTheParticle()
            self.moodTimer()
            self.startTheParticle()
        }
    }
    
    
}

