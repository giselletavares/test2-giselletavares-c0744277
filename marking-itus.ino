// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();
void setup() {
  button.begin();

  // Exposed functions-test

  // 1. showing the instructor mood
  // http://api.particle.io/<deviceid>/answer
  Particle.function("mood", showTheMood);
}

void loop() {
    
    
}

int DELAY = 1000;
int elapsedTime = 0;

int showTheMood(String cmd) {
    
  if (cmd != "start" || cmd != "stop"){
      int values = cmd.indexOf(" ");
      DELAY = (cmd.substring(0, values).toInt() * 1000) + 1000;
      elapsedTime = cmd.substring(values + 1).toInt();
      button.allLedsOff();
  }
    
  if (cmd != "stop") {
    for (int i = 21 - elapsedTime; i >= 0; i--) {
      
        if(i == 21){
            button.allLedsOff();
            button.ledOn(3, 0, 255, 0);
            button.ledOn(4, 0, 255, 0);
            button.ledOn(5, 0, 255, 0);
            button.ledOn(6, 0, 255, 0);
            button.ledOn(7, 0, 255, 0);
            button.ledOn(8, 0, 255, 0);
            button.ledOn(9, 0, 255, 0);
        } else if (i == 16) {
            button.allLedsOff();
            button.ledOn(4, 0, 255, 0);
            button.ledOn(5, 0, 255, 0);
            button.ledOn(6, 0, 255, 0);
            button.ledOn(7, 0, 255, 0);
            button.ledOn(8, 0, 255, 0);
        } else if (i == 12) {
            button.allLedsOff();
            button.ledOn(5, 0, 255, 0);
            button.ledOn(6, 0, 255, 0);
            button.ledOn(7, 0, 255, 0);
        } else if (i == 8) {
            button.allLedsOff();
            button.ledOn(6, 0, 255, 0);
        } else if (i == 4) {
            button.allLedsOff();
        } else if (i == 0) {
            button.allLedsOn(255,0,0);
        }
        
        delay(DELAY);
        
      }  
  }    
  
  if(cmd == "stop"){
      button.allLedsOff();
  }
  
  return 1;
}